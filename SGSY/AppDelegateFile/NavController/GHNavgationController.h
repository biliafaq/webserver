//
//  GHNavgationController.h
//  PlantingPod
//
//  Created by 苟应航 on 2019/8/1.
//  Copyright © 2019 mapelectric. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GHNavgationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
