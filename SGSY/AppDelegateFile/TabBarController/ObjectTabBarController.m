//
//  ObjectTabBarController.m
//  PlantingPod
//
//  Created by 苟应航 on 2019/8/1.
//  Copyright © 2019 mapelectric. All rights reserved.
//

#import "ObjectTabBarController.h"
#import "SGHomeViewController.h"
#import "SGMainController.h"
#import "GHNavgationController.h"
@interface ObjectTabBarController()

@end
@implementation ObjectTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];    [self addChildViewControllers];
}

//添加子控制器
- (void)addChildViewControllers{
    [self addChildrenViewController:[[SGHomeViewController alloc] init] andTitle:@"首页" andImageName:@"tab1"];
    [self addChildrenViewController:[[SGMainController alloc] init] andTitle:@"发现" andImageName:@"tab2"];
    [self addChildrenViewController:[[SGHomeViewController alloc] init] andTitle:@"商城" andImageName:@"tab3"];
    [self addChildrenViewController:[[SGMainController alloc] init] andTitle:@"我的" andImageName:@"tab4"];
}

- (void)addChildrenViewController:(UIViewController *)childVC andTitle:(NSString *)title andImageName:(NSString *)imageName{
    childVC.tabBarItem.image = [UIImage imageNamed:imageName];
    // 选中的颜色由tabbar的tintColor决定
    childVC.tabBarItem.selectedImage =  [UIImage imageNamed:imageName];
    childVC.title = title;
    
    GHNavgationController *baseNav = [[GHNavgationController alloc] initWithRootViewController:childVC];
    [self addChildViewController:baseNav];
}
@end
