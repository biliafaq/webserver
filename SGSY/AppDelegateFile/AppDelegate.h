//
//  AppDelegate.h
//  PlantingPod
//
//  Created by 苟应航 on 2019/7/25.
//  Copyright © 2019 mapelectric. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

