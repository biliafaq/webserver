//
//  CenterabBarController.h
//  PlantingPod
//
//  Created by 苟应航 on 2019/7/26.
//  Copyright © 2019 mapelectric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterTabBaer.h"
NS_ASSUME_NONNULL_BEGIN

@protocol CenterabBarControllerDelegate<UITabBarControllerDelegate>
// 重写了选中方法，主要处理中间item选中事件
- (void)ghTabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController;
@end

@interface CenterabBarController : UITabBarController
@property(nonatomic,weak) id<CenterabBarControllerDelegate> ghDelegate;
@property(nonatomic,strong)CenterTabBaer *ghTabbar;
@end

NS_ASSUME_NONNULL_END
