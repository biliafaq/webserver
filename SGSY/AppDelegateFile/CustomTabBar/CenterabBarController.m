//
//  CenterabBarController.m
//  PlantingPod
//
//  Created by 苟应航 on 2019/7/26.
//  Copyright © 2019 mapelectric. All rights reserved.
//

#import "CenterabBarController.h"

@interface CenterabBarController ()<UITabBarControllerDelegate>

@end

@implementation CenterabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    _ghTabbar=[[CenterTabBaer  alloc]init];
    [_ghTabbar.centerBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self setValue:_ghTabbar forKeyPath:@"tabBar"];
    self.delegate = self;
    // Do any additional setup after loading the view.
}

// 重写选中事件， 处理中间按钮选中问题
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    _ghTabbar.centerBtn.selected = (tabBarController.selectedIndex == self.viewControllers.count/2);
    
    if (self.ghDelegate){
        [self.ghDelegate ghTabBarController:tabBarController didSelectViewController:viewController];
    }
}

- (void)buttonAction:(UIButton *)button{
    NSInteger count = self.viewControllers.count;
    self.selectedIndex = count/2;//关联中间按钮
    [self tabBarController:self didSelectViewController:self.viewControllers[self.selectedIndex]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
