//
//  SGGuideViewController.m
//  SGSY
//
//  Created by 苟应航 on 2019/8/19.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import "SGGuideViewController.h"
#import "ObjectTabBarController.h"
#import "GHNavgationController.h"
@interface SGGuideViewController ()<UIScrollViewDelegate>
{
    UIPageControl *pageControl;
    UIScrollView *guideScroll;
    UIButton *_tiaoguobtn;
    UIButton *_longtbs;
}
@property (nonatomic , strong) NSArray *picArr;


@end

@implementation SGGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _picArr = @[
                @"new_0",
                @"new_1",
                @"new_2",];
    
    [self loadContentView];
    [UIApplication sharedApplication].statusBarHidden = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    //    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

/**
 *  初始化界面
 */
- (void)loadContentView {
    guideScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT == 812.0 ? -44 : -22, SCREEN_WIDTH, SCREEN_HEIGHT+(SCREEN_HEIGHT==812.0?44:22))];
    guideScroll.delegate = self;
    [self.view addSubview:guideScroll];
    guideScroll.showsHorizontalScrollIndicator=NO;
    guideScroll.contentSize = CGSizeMake(SCREEN_WIDTH*self.picArr.count, 0);
    guideScroll.userInteractionEnabled = YES;
    guideScroll.bounces = NO;
    guideScroll.pagingEnabled = YES;
    
    
    NSMutableArray *imgAry=[NSMutableArray array];
    
    for (int i=0; i< _picArr.count;i++) {
        NSString *imgName = _picArr[i];
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*i, 0, SCREEN_WIDTH, [UIScreen mainScreen].bounds.size.height)];
        imgView.image=SGImage(imgName);
        
        [guideScroll addSubview:imgView];
        imgView.userInteractionEnabled = YES;
        imgView.contentMode=UIViewContentModeScaleAspectFill;
        imgView.clipsToBounds=YES;
        [imgAry addObject:imgView];
        
    }
    
    UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-80, SCREEN_HEIGHT==812.0?50:40, 50, 30*SCALEWITH6)];
    [btn setTitle:@"跳过" forState:UIControlStateNormal];
    
    [btn setTitleColor:[UIColor colorWithHexString:@"#ff6666" andAlpha:1] forState:(UIControlState)UIControlStateNormal];
    btn.titleLabel.font=SGFont(12);
    btn.layer.cornerRadius=2;
    btn.hidden=YES;
    btn.layer.borderColor=[UIColor colorWithHexString:@"#ff6666" andAlpha:1].CGColor;
    btn.layer.borderWidth=1;
    btn.tag=100;
    [btn addTarget:self action:@selector(startApp:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    _tiaoguobtn=btn;
    
    UIButton *btns=[[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-75, SCREEN_HEIGHT-70*SCALEWITH6, 150, 30*SCALEWITH6)];
    [btns setTitle:@"立即体验" forState:UIControlStateNormal];
    [btns setTitleColor:SGwhite forState:UIControlStateNormal];
    btns.titleLabel.font=SGFont(12);
    btns.tag=101;
    btns.hidden=YES;
    [btns addTarget:self action:@selector(startApp:) forControlEvents:UIControlEventTouchUpInside];
    btns.backgroundColor=[UIColor colorWithHexString:@"#ff6666" andAlpha:1];
    btns.layer.cornerRadius=(30*SCALEWITH6)/2;
    [self.view addSubview:btns];
    _longtbs=btns;
    //    [[imgAry lastObject] addSubview:vies];
}
/**
 *  启动app
 */
- (void)startApp:(UIButton *)sender
{
    ObjectTabBarController *homevc= [[ObjectTabBarController alloc]init];
    [UIApplication sharedApplication].keyWindow.rootViewController = homevc;
    
}

#pragma UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int currentPage=guideScroll.contentOffset.x;
    if (currentPage==SCREEN_WIDTH*2) {
        _tiaoguobtn.hidden=NO;
        _longtbs.hidden=NO;
    }else{
        _tiaoguobtn.hidden=YES;
        _longtbs.hidden=YES;
    }
    //    int currentPage = (guideScroll.contentOffset.x - SCREEN_WIDTH
    //                       / ([_picArr count]+1)) / SCREEN_WIDTH + 1;
    //
    //    pageControl.currentPage = currentPage;
    
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    //    XTDTabBarController *tabs = [XTDTabBarController new];
    //
    //    [UIApplication sharedApplication].keyWindow.rootViewController = tabs;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

