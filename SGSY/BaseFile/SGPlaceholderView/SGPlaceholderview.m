//
//  SGPlaceholderview.m
//  SGSY
//
//  Created by 时光岁月 on 2019/8/21.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import "SGPlaceholderview.h"

@implementation SGPlaceholderview
-(instancetype)initWithReminder:(NSString *)reminder andImage:(UIImage *)image{
    if (self=[super init]) {
        self.userInteractionEnabled=YES;
        [self setUI:reminder andImage:image];
    }
    return self;
}
-(void)setUI:(NSString *)reminder andImage:(UIImage *)image{
    self.Placeholdimg = [[UIImageView alloc]init];
    self.Placeholdimg.image=image;
    [self addSubview:self.Placeholdimg];
    [self.Placeholdimg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-64);
    }];
    
    
    UILabel * tiplbel = [[UILabel alloc] init];
    tiplbel.text=reminder;
    tiplbel.textColor=KD8D8D8Color;
    tiplbel.textAlignment=1;
    tiplbel.font = [UIFont fontWithName:@"Ping Fang SC" size:14];
    [self addSubview:tiplbel];
    [tiplbel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.Placeholdimg.mas_bottom).offset(-40);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.mas_equalTo(20);
    }];
    
    
    self.aginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.aginBtn setTitleColor:kWhiteColor forState:UIControlStateNormal];
    [self.aginBtn setTitle:@"重新加载" forState:UIControlStateNormal];
    [self.aginBtn setBackgroundColor:kRedColor];
    self.aginBtn.layer.cornerRadius =5;
    self.aginBtn.layer.masksToBounds=YES;
    [self.aginBtn addTarget:self action:@selector(addclick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.aginBtn];
    [self.aginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tiplbel.mas_bottom).offset(20);
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(60);
    }];
    
    
}

-(void)addclick{
    
    if (self.refreshBlock) {
        self.refreshBlock();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
