//
//  SGPlaceholderview.h
//  SGSY
//
//  Created by 时光岁月 on 2019/8/21.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^RefreshDataBlock)(void);
@interface SGPlaceholderview : UIView
/*
 占位图
 */
-(instancetype)initWithReminder:(NSString *)reminder andImage:(UIImage *)image;
@property (nonatomic,strong)UIImageView *Placeholdimg;
@property (nonatomic,strong)UIButton *aginBtn;
@property (nonatomic,copy) RefreshDataBlock  refreshBlock;
@end

NS_ASSUME_NONNULL_END
