//
//  SGMethonTool.m
//  SGSY
//
//  Created by 时光岁月 on 2019/8/20.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import "SGMethonTool.h"

@implementation SGMethonTool
+ (instancetype)sharedInstance{
    static SGMethonTool * _tool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        _tool = [[SGMethonTool alloc]init];
    });
    return _tool;
}
@end
