//
//  AppdelegateTool.h
//  SGSY
//
//  Created by 时光岁月 on 2019/8/20.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppdelegateTool : NSObject
-(void)makeKeyBoardManager;
@end

NS_ASSUME_NONNULL_END
