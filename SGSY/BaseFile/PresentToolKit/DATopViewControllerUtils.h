//
//  DATopViewControllerUtils.h
//  SGSY
//
//  Created by 时光岁月 on 2019/8/20.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class UINavigationController, UIViewController;
@interface DATopViewControllerUtils : NSObject
/**
 获取当前最顶层导航控制器
 
 @return 导航控制器
 */
+ (UINavigationController *)topmostNavigationController;

/**
 获取当前最顶层控制器
 
 @return 控制器
 */
+ (UIViewController *)topmostViewController;

@end

NS_ASSUME_NONNULL_END
