//
//  CustomButton.h
//  SGSY
//
//  Created by 时光岁月 on 2019/8/20.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger) {
    YSLCustomButtonImageTop    = 0 , //图片在上边
    YSLCustomButtonImageLeft   = 1 , //图片在左边
    YSLCustomButtonImageBottom = 2 , //图片在下边
    YSLCustomButtonImageRight  = 3   //图片在右边
}YSLCustomButtonType;
@interface CustomButton : UIButton
/** 图片和文字间距 默认10px*/
@property (nonatomic , assign) CGFloat ysl_spacing;

/** 按钮类型 默认YSLCustomButtonImageTop 图片在上边*/
@property (nonatomic , assign) YSLCustomButtonType ysl_buttonType;
@end

NS_ASSUME_NONNULL_END
