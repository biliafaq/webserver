//
//  BaseTopLabel.h
//  SGSY
//
//  Created by 时光岁月 on 2019/8/20.
//  Copyright © 2019 SGSY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum VerticalAlignment {
    VerticalAlignmentTop,
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;
@interface BaseTopLabel : UILabel
{
@private
    VerticalAlignment _verticalAlignment;
}
@property (nonatomic) VerticalAlignment verticalAlignment;
-(void)setVerticalAlignment:(VerticalAlignment)verticalAlignment;
@end

NS_ASSUME_NONNULL_END
