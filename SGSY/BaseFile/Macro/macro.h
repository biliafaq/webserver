//
//  macro.h
//  SGSY
//
//  Created by 时光岁月 on 2019/8/20.
//  Copyright © 2019 SGSY. All rights reserved.
//

#ifndef macro_h
#define macro_h
#import <PYSearch/PYSearch.h>
/**以6s为标准，定义一个宽度比例，用于计算动态高度*/
#define DEVICE_Sales  DEVICE_WIDTH/375
///状态栏高度
#define KStatusHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define kIs_iphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kIs_iPhoneX kScreenWidth >=375.0f && kScreenHeight >=812.0f&& kIs_iphone

/*状态栏高度*/
#define kStatusBarHeight (CGFloat)(kIs_iPhoneX?(44.0):(20.0))
/*导航栏高度*/
#define kNavBarHeight (44)
/*状态栏和导航栏总高度*/
#define kNavBarAndStatusBarHeight (CGFloat)(kIs_iPhoneX?(88.0):(64.0))
/*TabBar高度*/
#define kTabBarHeight (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))
/*顶部安全区域远离高度*/
#define kTopBarSafeHeight (CGFloat)(kIs_iPhoneX?(44.0):(0))
/*底部安全区域远离高度*/
#define kBottomSafeHeight (CGFloat)(kIs_iPhoneX?(34.0):(0))
/*iPhoneX的状态栏高度差值*/
#define kTopBarDifHeight (CGFloat)(kIs_iPhoneX?(24.0):(0))
/*导航条和Tabbar总高度-----*/
#define kNavAndTabHeight (kNavBarAndStatusBarHeight + kTabBarHeight)
#define kWindow [UIApplication sharedApplication].keyWindow

#pragma makr - Color

#define kBlackColor       [UIColor blackColor]
#define kDarkGrayColor    [UIColor darkGrayColor]
#define kLightGrayColor   [UIColor lightGrayColor]
#define kWhiteColor       [UIColor whiteColor]
#define kRedColor         [UIColor redColor]
#define kBlueColor        [UIColor blueColor]
#define kGreenColor       [UIColor greenColor]
#define kCyanColor        [UIColor cyanColor]
#define kYellowColor      [UIColor yellowColor]
#define kMagentaColor     [UIColor magentaColor]
#define kOrangeColor      [UIColor orangeColor]
#define kPurpleColor      [UIColor purpleColor]
#define kBrownColor       [UIColor brownColor]
#define kClearColor       [UIColor clearColor]

#define kF6F6F6Color      [UIColor colorWithHexString:@"F6F6F6" andAlpha:1.0f]
#define kFFCC3CColor      [UIColor colorWithHexString:@"FFCC3C" andAlpha:1.0f]
#define kF8F8F8Color      [UIColor colorWithHexString:@"F8F8F8" andAlpha:1.0f]
#define k666666Color      [UIColor colorWithHexString:@"666666" andAlpha:1.0f]
#define kECEFF3Color      [UIColor colorWithHexString:@"ECEFF3" andAlpha:1.0f]
#define kFFC21DColor      [UIColor colorWithHexString:@"FFC21D" andAlpha:1.0f]
#define k23242AColor      [UIColor colorWithHexString:@"23242A" andAlpha:1.0f]//CCC8C2
#define kCCC8C2Color      [UIColor colorWithHexString:@"CCC8C2" andAlpha:1.0f]//D0021B
#define kD0021BColor      [UIColor colorWithHexString:@"D0021B" andAlpha:1.0f]
#define KFAFAFAColor      [UIColor colorWithHexString:@"FAFAFA" andAlpha:1.0f]//3A0202
#define K3A0202Color      [UIColor colorWithHexString:@"3A0202" andAlpha:1.0f]
#define KFFFFFFColor       [UIColor colorWithHexString:@"FFFFFF" andAlpha:1.0f]
#define K999999Color       [UIColor colorWithHexString:@"999999" andAlpha:1.0f]//CCC3C2
#define KCCC3C2Color       [UIColor colorWithHexString:@"CCC3C2" andAlpha:1.0f]//FFCC3C
#define KEDEDEDColor       [UIColor colorWithHexString:@"EDEDED" andAlpha:1.0f]//E73E32
#define KE73E32Color       [UIColor colorWithHexString:@"E73E32" andAlpha:1.0f]//E73E32  979797
#define K979797Color       [UIColor colorWithHexString:@"979797" andAlpha:1.0f]//E73E32  979797  FFCC3C
#define KD8D8D8Color       [UIColor colorWithHexString:@"D8D8D8" andAlpha:1.0f]//C3B3A8
#define KC3B3A8Color       [UIColor colorWithHexString:@"C3B3A8" andAlpha:1.0f]//35D07D
#define K35D07DColor       [UIColor colorWithHexString:@"35D07D" andAlpha:1.0f]//6CB0FF
#define K6CB0FFColor       [UIColor colorWithHexString:@"6CB0FF" andAlpha:1.0f]//6CB0FF

//   
// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

#endif /* macro_h */
